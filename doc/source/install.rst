Building and installing
-----------------------

Download
........

Get the git repository: In a directory where you have write permission:

    ``$> git clone https://gitlab.gwdg.de/mpievolbio-scicomp/gillespiesimulator.git``

For the following, we assume your current working directory is the top level directory of
the distribution. To change into the new directory containing the source code:

    ``$> cd gillespiesimulator``


Dependencies
............

We recommend conda (https://conda.readthedocs.io/en/latest/) to manage the dependencies.
A conda environment is part of the distribution. To build it:

    ``$> conda env create -f environment.yml``

Activate the new environment (named "rq" unless you choose a different name):

    ``$> conda activate rq``

If the last command fails, simply follow the instructions written on the terminal.

Installation
............

To install, use pip:

    ``$> pip install .``

Test
....

Although not strictly required, we recommend to run the test suite:

    ``$> python test/ ``



