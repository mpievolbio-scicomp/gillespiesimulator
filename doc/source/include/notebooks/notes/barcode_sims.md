# Testing fitness estimates with simulated barcode lineage cell counts.
We use the code [pyfitseq](https://github.com/FangfeiLi05/PyFitSeq.git) to
analyse a virtual long term evolution experiment based on a stochastic
simulation of a non--sexually reproducing bacterial colony tagged with genetic barcode
sequences. Simulations are performed with a python implementation of the 
Gillespie stochastic simulation algorithm, described
[here](https://gitlab.gwdg.de/mpievolbio-scicomp/gillespiesimulator). 

## Case 1: Three lineages, constant competition matrix, no cell death, no
mutations.

The jupyter notebook for this basic test case is available at 
[https://gitlab.gwdg.de/mpievolbio-scicomp/gillespiesimulator/-/blob/master/doc/source/include/notebooks/fitseq_test_CMconst.ipynb](https://gitlab.gwdg.de/mpievolbio-scicomp/gillespiesimulator/-/blob/master/doc/source/include/notebooks/fitseq_test_CMconst.ipynb)

The starting population consists of three barcode lineages with 10 cells per
lineage. They grow with an average division rate of 1 per generation, the death
rate is set to zero here. Mutations do never occur.
The competition matrix was initialized as

[[ 10^-9 10^-2 10^-2],  
 [ 10^-9 10^-9 10^-2],   
 [ 10^-9 10^-9 10^-9]] 


Accordingly, barcode #2 is expected to
outperform barcodes #0 and #1 and barcode #1 to survive longer than barcode #0.
Absolute cell counts are limited to 10000 through the carrying capacity.

A typical trajectory (cell counts vs. time) is shown here:  
![Image](img/image_2020-06-17-10-35-09.png)  
**Trajectory of mutants, barcode lineages, and number of mutations (right
panels) for Case 1. The left panels shows heatmaps of the competition matrix.**


We analysed this data with `fitseq`. `fitseq` estimates the Wrightian fitness
coefficient of each lineage given the cell counts (i.e. barcode read counts) per
lineage at the various observation timepoints. Besides the fitness estimate,
`fitseq` also reports the estimated cell counts for each lineage and timepoint
corresponding to the statistical model underlying the `fitseq` algorithm. In the subsequent
analysis, we compare these count estimates to our simulation data. The agreement
between `fitseq`'s output and our simulation data serves as a
proxy for the *meaningfulness* of the estimated fitness coefficients. Later, we
will try to establish a connection of estimated fitness coefficients to the
competition matrix.

In our test case with three unmutable barcode lineages, `fitseq`
accurately reproduced the cell counts as shown here:  
![Image](img/15.png)    
**`fitseq` estimated cell counts vs. simulation data for three lineages without
mutations.**

## Case 2: Including mutations

Next, we allowed mutations to occur at a 10% rate at every cell division. The
competition matrix values are drawn from a normal distribution with mean 0.02
and standard deviation 0.01. 
The jupyter notebook is available at
[https://gitlab.gwdg.de/mpievolbio-scicomp/gillespiesimulator/-/blob/master/doc/source/include/notebooks/fitseq_test_CMvar.ipynb](https://gitlab.gwdg.de/mpievolbio-scicomp/gillespiesimulator/-/blob/master/doc/source/include/notebooks/fitseq_test_CMvar.ipynb)

A typical run is shown below:  
![Image](img/image_2020-06-18-14-39-51.png)  
**Trajectory of mutants, barcode lineages, and number of mutations (right
panels) for Case 2. The left panels shows heatmaps of the competition matrix.**

The barcode lineage #1 dominates the
population. It develops a seemingly advantageous mutation early on in time
(t=1). Another even more advantageous mutation occurs at t=3 which eventually
swipes through the entire population.

Feeding the data into `fitseq` yields the following estimated cell counts:   
![Image](img/image_2020-06-18-14-42-42.png)  
**Estimated cell counts vs. simulation data for three lineages with
mutations.**

Fitseq performs sufficiently well only for the most abundant barcode lineage,
whereas it deviates strongly for the less prolific lineages. We attribute this
observation to the fact that the competiton matrix is non-constant giving
individual lineages the opportunity to develop competitive advantages or
disadvantages with respect to the other lineages in a disruptive manner.
This dynamic behaviour is not reflected in the static fitness coefficient
inferred by `fitseq`.


## Case 3: 100 lineages with mutations.
We observed that `fitseq` performs better under conditions where there are much
more barcode lineages than obervation snapshots. Therefore, we performed a
simulation with 100 lineages, giving 10 cells per lineage to the starting population.
Mutations occur with a rate of 1% per generation and effect the competition
matrix only. The initial competition matrix is drawn from a normal distribution with
mean 2e-4 and standard deviation 1e-2. The carrying capacity is set to 1e6 and
the simulation runs until t=10 generations.

Notebook available at 
[https://gitlab.gwdg.de/mpievolbio-scicomp/gillespiesimulator/-/blob/master/doc/source/include/notebooks/fitseq_test_CMvar_100lineages.ipynb](https://gitlab.gwdg.de/mpievolbio-scicomp/gillespiesimulator/-/blob/master/doc/source/include/notebooks/fitseq_test_CMvar_100lineages.ipynb)

For the subsequent analysis, we took only the 9 "best survivor" lineages into
account, i.e. the lineages with the highest cell counts at the last timepoint in
our simulation. These cell counts and the respective estimates from `fitseq` are
shown below:  
![Image](img/image_2020-06-17-15-19-16.png)  
**Estimated and simulated cell counts for the 10 most prolific barcode lineages
(Case 3).** 
    
we find again that `fitseq` performs rather well for the most abundant lineages
but deteriorates for lineages with smaller cell counts to which `fitseq`
attributes lower fitness coefficients. Moreover, even for the most prolific
lineages, `fitseq` fails to yield accurate estimates if sudden mutations occur as
for example in lineage #70 (red line) in the figure above.

In the figure below , we plot the estimated cell counts vs. the simulated data
for each of the 9 "best survivors":  
![Image](img/image_2020-06-17-15-24-28.png)  
**Cell count estimates from `fitseq` vs. actual (simulated) cell counts for the 9
most prolific barcode lineages**

### Extracting a dynamic fitness estimate
We now return to the question to what degree a static fitness coefficient can
accurately characterize the population dynamic in the case of sudden changes in
the competition matrix. Let's take a look at the most prolific lineage, barcode
51. The figure below plots the cell counts as estimated from `fitseq` and the
simulated cell counts as a function of time:  
![Image](img/image_2020-06-18-15-45-00.png)  
** Estimated vs. simulated cell counts for most prolific barcode lineage #51. **

The estimated (Wright) fitness for lineage 51 is $x_w = 2.56$. 

To provide a dynamic fitness coefficient, we perform `fitseq` analysis on a
sliding time window: Instead of feeding the entire time series of lineage cell
counts into `fitseq`, we take data only from the time interval $[t, t + \tau]$ with $\tau$
being the window width. Incrementing $t$ from $t=0$ to $t=10-\tau$ then yields a
time series of fitness coefficients for each lineage and for each time interval. As a proxy for how well
this *sliding* fitness characterizes the dynamics, we observe again the
estimated cell counts obtained for each interval. The results are shown
below for a window of $\tau = 2$.    
![Image](img/image_2020-06-18-16-17-54.png)  
**Simulated cell counts compared to fitseq estimates. Dashed line: fitseq
estimates for the entire time series, stars: fitseq estimates for sliding window
of length $\tau=3.**

We find that the 'sliding window' method yields a much better agreement between
estimated cell counts from `fitseq` and the actual (simulated) cell counts. We
take this as a strong hint that the corresponding time dependent fitness
coefficients provide a more robust characterization of the population dynamics
under changing competitiveness among lineages than taking a sirgle constant fitness
per lineage for the entire history.

Now let's infer the *sliding* fitness for all 9 most prolific lineages and
compare to the corresponding competition matrix elements.

![Image](img/image_2020-06-18-22-05-43.png)   
**Wright fitness vs. time interval using the  *sliding window* method.**

The heatmap of the competition matrix elements for the 9 most prolific lineages,
summed over all competition parters (matrix rows) is shown below:  

![Image](img/image_2020-06-18-22-08-59.png)  
**Competition matrix summed over rows for the 9 most prolific barcode
lineages.**

At first sight, there is no directly visible relation between the *sliding
window* fitness and the competition matrix. Further investigations are in order
.... ;)
 
