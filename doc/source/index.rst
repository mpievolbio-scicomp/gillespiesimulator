.. GillespieSimulator documentation master file, created by
   sphinx-quickstart on Tue Aug 13 08:49:55 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

GillespieSimulator's documentation
==================================

.. toctree::
   :maxdepth: 3
   :caption: Contents:

   install.rst
   background.rst
   refman.rst
   /include/notebooks/GillespieExample.ipynb
   /include/notebooks/GillespieExample-multiprocessing.ipynb
   /include/notebooks/LogisticGrowth.ipynb
   /include/notebooks/CyclicDominance.ipynb
   /include/notebooks/notes/barcode_sims.md




License
-------
.. image:: https://i.creativecommons.org/l/by-sa/4.0/88x31.png
    :target: http://creativecommons.org/licenses/by-sa/4.0/
    :alt: Creative Commons License

This documentation is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License.

The software is licensed under the MIT license. See LICENSE_ in the project's root directory.

.. _LICENSE: https://gitlab.gwdg.de/c.fortmanngrote/gillespiesimulator/blob/master/LICENSE

Source code repository
----------------------
The source code as well as this documentation are maintained at
https://gitlab.gwdg.de/mpievolbio-scicomp/gillespiesimulator .

Test and build status
---------------------
|pipeline status|

.. |pipeline status| image:: https://gitlab.gwdg.de/mpievolbio-scicomp/gillespiesimulator/badges/master/pipeline.svg
   :target: https://gitlab.gwdg.de/mpievolbio-scicomp/gillespiesimulator/commits/master

Applications
------------

.. mdinclude:: /include/notebooks/notes/barcode_sims.md


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
