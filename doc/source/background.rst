Background information
----------------------

The Gillespie algorithm (Gillespie1977_)
is a stochastic simulation for reacting agents, e.g. chemical mixtures or
competing organisms. It takes an initial population (number of individuals of each species)
and reaction rates as parameters and propagates the population
through time. In each iteration of the algorithm, the time at which the next reaction occurs and the type of reaction
is chosen in accordance with the currently present population and reaction rates. Hence, the Gillespie algorithm is a
non-constant time step algorithm.

In this implementation, we focus on population dynamics of non-sexually reproducing cells, e.g. bacteria. The
central object is the Gillespie Agent. An agent represents a cell type, which is characterized by a reproduction rate
(cell division rate)the database,
a death rate, and a mutation rate, as well as a competition vector, which determines its relative fitness
against other cell types in the population. The set of all competition vectors constitutes the competition matrix.

Furthermore, we assign a unique cell type ID to each agent in the initial population.
In this way, a Gillespie run mimics a growth experiment with *barcoded bacteria*.
Finally, the population size and a mutation ID
complete the list of agent properties.

With this object oriented approach, we make it possible to track the emergence and disappearance of
new cell types through mutation over a complete simulation and to quickly decipher genealogies and ancestral
relationships.

Our Gillespie process contains the following reactions among
agents :math:`A_i`

    * Division: :math:`A_i \rightarrow A_i + A_i`, division rate :math:`[\lambda_i+(\lambda_i-\theta)N_{cap}](1-\mu)`
    * Mutation: :math:`A_i\rightarrow A_i + A_j`, mutation rate :math:`[\lambda_i+(\lambda_i-\theta)N_{cap}] \mu`
    * Death:    :math:`A_i \rightarrow ()`, death rate :math:`d_i - (d_i - \theta)N_{cap}`
    * Defeat in competition:   :math:`A_i + A_j \rightarrow A_j`, competition rate :math:`c_{ij}`

Here, :math:`\lambda_i` is the division rate ande :math:`\mu\in[0,1]` is the probability that a mutation occurs during the cell division.
The :math:`c_{ij}` are the elements of the competition matrix. :math:`N_{cap}` is the carrying capacity and :math:`\theta` controls my how much the population can overshoot the carrying capacity (???, need a reference here).

The time of next reaction and the reaction type are calculated from the
reaction rates and the population strength of each species. As an
intermediate step, we first calculate the reaction propensities*, i.e. the probabilities
that in the time interval :math:`[t,t+\tau]` and a with a given population distribution :math:`\{n_i\}_{i=1}^{N}`
a reaction of a certain type occurs.
Here, :math:`i` enumerates the :math:`N` cell types,
The conditional probabilities :math:`\Omega` are given by

    * Division: :math:`\Omega_{i}^\parallel = n_i [\lambda_i+(\lambda_i-\theta)N_{cap}](1-\mu)`
    * Mutation: :math:`\Omega_{i}^* = n_i[\lambda_i+(\lambda_i-\theta)N_{cap}]\mu`
    * Death:    :math:`\Omega_{i}^\dagger = n_i [d_i - (d_i - \theta)N_{cap}]`
    * Defeat:   :math:`\Omega_{i}^{\times} = \sum_{i}\left[n_i n_j-\frac{1}{2}\delta_{ij}n_i\left(n_i + 1\right)\right]\cdot c_{ij}`.

The "defeat" term accounts for the different statistical weights
depending on whether the competition it between individuals of the same lineage or between two different lineages.

Upon cell division or cell death, the population sizes of the respective agent
are incremented or decremented, respectively. Upon mutation, a new cell type (agent) is
created with initial population size 1. Division rate, death rate, and competition matrix elements
for the new agent are drawn from a normal distribution centered on the
initially fixed simulation parameters. The mutation
rate is kept constant throughout the simulation. The agent ID is inherited from the parent (in the
same way as a genomic barcode sequence is inherited to offspring individuals) and a new mutation ID is
assigned.

Finally, upon defeat in competition against another cell type, the population size of the defeated
cell type is decremented.

### TODO: Add note on Lotka-Volterra dynamics and relation to GSSA parameters. See mpb labseminar talk on Aug. 8 2020.

.. _Gillespie1977: https://doi.org/10.1021/j100540a008


Mutations
.........

(Point) Mutations may occur anywhere in the genome, including the barcode sequence.
Mutations that in the barcode sequence occur at a rate given by the ratio of the length
of the barcode to the genome size. In our simulation, we treat barcode mutations by
assigning a new `cell_id` to the offspring while the `mutation_id` as well as
all other properties are inherited from the parent cell. To identify cells with
barcode mutations in the data postprocessing analysis, it would hence suffice to
filter all `cell_ids` beyond the initial cells in the population array and
to identify their parents via the `mutation_id`.
