# GillespieSimulator

An object-oriented implementation of the stochastic
reaction simulation algorithm by [[Gillespie 1977]](https://doi.org/10.1021/j100540a008)
in python.

DISCLAIMER: THIS IS WORK IN PROGRESS. EXPECT ALL RESULTS TO BE WRONG!

Installation instructions, reference manual, and examples at https://mpievolbio-scicomp.pages.gwdg.de/gillespiesimulator

Build and test status: [![pipeline status](https://gitlab.gwdg.de/mpievolbio-scicomp/gillespiesimulator/badges/master/pipeline.svg)](https://gitlab.gwdg.de/mpievolbio-scicomp/gillespiesimulator/commits/master)

## Examples:
### [Basic functionality](https://gitlab.gwdg.de/mpievolbio-scicomp/gillespiesimulator/-/blob/master/doc/source/include/notebooks/GillespieExample.ipynb)  [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.gwdg.de%2Fmpievolbio-scicomp%2Fgillespiesimulator.git/master?filepath=doc%2Fsource%2Finclude%2Fnotebooks%2FGillespieExample.ipynb)
### [Multiprocessing example](https://gitlab.gwdg.de/mpievolbio-scicomp/gillespiesimulator/-/blob/master/doc/source/include/notebooks/GillespieExample-multiprocessing.ipynb)  [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.gwdg.de%2Fmpievolbio-scicomp%2Fgillespiesimulator.git/master?filepath=doc%2Fsource%2Finclude%2Fnotebooks%2FGillespieExample-multiprocessing.ipynb)
### [Logistic growth](https://gitlab.gwdg.de/mpievolbio-scicomp/gillespiesimulator/-/blob/master/doc/source/include/notebooks/LogisticGrowth.ipynb)  [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.gwdg.de%2Fmpievolbio-scicomp%2Fgillespiesimulator.git/master?filepath=doc%2Fsource%2Finclude%2Fnotebooks%2FLogisticGrowth.ipynb)
### [Cyclic dominance](https://gitlab.gwdg.de/mpievolbio-scicomp/gillespiesimulator/-/blob/master/doc/source/include/notebooks/CyclicDominance.ipynb)  [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.gwdg.de%2Fmpievolbio-scicomp%2Fgillespiesimulator.git/master?filepath=doc%2Fsource%2Finclude%2Fnotebooks%2FCyclicDominance.ipynb)
