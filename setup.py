from setuptools import setup

setup(
    name='GillespieSimulator',
    use_scm_version=True,
    setup_requires=['setuptools_scm'],
    packages=['GillespieSimulator',
             ],
    license='MIT',
    #long_description=open('README.md').read(),
)
