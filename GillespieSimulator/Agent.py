""" :module Agent: Module to host the Agent class."""

# Imports
import numpy
import logging

class Agent(object):
    """ :class Agent: Class that implements a reactant cell type in a Gillespie simulation. """

    def __init__(self,
            cell_id=None,
            mutation_id=None,
            parent_mutation_ids=None,
            date_of_birth=None,
            number_of_copies=None,
            division_rate=None,
            death_rate=None,
            ):
        """ Constructor of the Agent class.

        :param cell_id: The identifier of this type.
        :type  cell_id: int

        :param mutation_id: An identifier for this individual's mutation. (0 -> wild type, >0 -> mutant)
        :type  mutation_id: int

        :param parent_mutation_ids: Mutation id of parental cell type.
        :type  parent_mutation_id: list

        :param date_of_birth: The simulation time step at which this individual appeared.
        :type  date_of_birth: float

        :param number_of_copies: The number of indiviuals of this type.
        :type  number_of_copies: int

        :param division_rate: The rate (number per unit time) at which cell divisions occurs for this type.
        :type  division_rate: float

        :param death_rate: The rate at which cells of this type die (not accounting for death through competition).
        :type  death_rate: float

        """

        self.cell_id = cell_id
        self.mutation_id = mutation_id
        self.parent_mutation_ids = parent_mutation_ids
        self.date_of_birth = date_of_birth
        self.number_of_copies = number_of_copies
        self.division_rate = division_rate
        self.death_rate = death_rate

        logging.debug("Agent created")

    @property
    def cell_id(self):
        '''The cell type identifier.'''
        return self.__cell_id
    @cell_id.setter
    def cell_id(self, val):
        if val is None:
            val = 0
            if val < 0:
                raise AttributeError("Parameter 'cell_id' must be a non-negative integer.")

        self.__cell_id = val

        logging.debug("Set cell_id to %d.", val)

    @property
    def mutation_id(self):
        ''' The mutation identifier of this type.'''
        return self.__mutation_id
    @mutation_id.setter
    def mutation_id(self, val):
        if val is None:
            val = 0
        if not isinstance(val, int):
            raise AttributeError("Parameter 'mutation_id' must be a non-negative integer.")
        if val < 0:
            raise AttributeError("Parameter 'mutation_id' must be a non-negative integer.")

        self.__mutation_id = val

        logging.debug("Set mutation_id to %d.", val)

    @property
    def parent_mutation_ids(self):
        '''The list of ancestral mutations.'''
        return self.__parent_mutation_ids
    @parent_mutation_ids.setter
    def parent_mutation_ids(self, val):
        if val is None:
            val = []

        self.__parent_mutation_ids = val

        logging.debug("Set parent_mutation_ids to %s.", str(val))

    @property
    def date_of_birth(self):
        '''The time at which the first individual of this type appeared.'''
        return self.__date_of_birth
    @date_of_birth.setter
    def date_of_birth(self, val):
        if val is None:
            val = 0

        self.__date_of_birth = val
        logging.debug("Set date_of_birth to %f.", val)

    @property
    def number_of_copies(self):
        '''The number of individuals of this type.'''
        return self.__number_of_copies
    @number_of_copies.setter
    def number_of_copies(self, val):
        if val is None:
            val = 1

        self.__number_of_copies = val
        logging.debug("Set number_of_copies to %d.", val)

    @property
    def division_rate(self):
        '''The rate at which individuals of this type divide.'''
        return self.__division_rate
    @division_rate.setter
    def division_rate(self, val):
        if val is None:
            val = 1.0

        self.__division_rate = val
        logging.debug("Set division_rate to %f.", val)

    @property
    def death_rate(self):
        '''The rate at which individuals of this type die.'''
        return self.__death_rate
    @death_rate.setter
    def death_rate(self, val):
        if val is None:
            val = 0.0

        self.__death_rate = val
        logging.debug("Set death_rate to %f.", val)

    def divide(self):
        """ Perform a cell division, increment the number of cells. Increases the number of copies by one. """

        self.number_of_copies += 1
        logging.debug("Cell division\n cell_id=%d, mutation_id=%d.", self.cell_id, self.mutation_id)

    def mutate(self, date, mutation, division_rate, death_rate):
        """ Create a mutant from this cell type and assign new properties. The new type will have the same cell_id as the parent but a new mutation_id.

        :param date: The birth date of the first individual of the new type.
        :type  date: float

        :param mutation: The mutation_id of the new cell type.
        :type  mutation: int

        :param division_rate: The division rate of the new cell type.
        :type  division_rate: float

        :param death_rate: The death rate of the new cell type.
        :type  death_rate: float

        :returns: The new cell type.
        :rtype: Agent

        """

        mutant = self.__class__(cell_id=self.cell_id,
                     mutation_id=mutation,
                     parent_mutation_ids=self.parent_mutation_ids + [self.mutation_id],
                     date_of_birth=date,
                     division_rate=division_rate,
                     death_rate=death_rate,
                     )

        logging.debug("Mutation.\n Cell_id=%d, mutation_id=%d.", mutant.cell_id, mutant.mutation_id)

        return mutant

    def die(self):
        """ Remove one cell."""

        if self.number_of_copies > 0:
            self.number_of_copies -= 1

        logging.debug("Death.\n cell_id=%d, mutation_id=%d.", self.cell_id, self.mutation_id)

