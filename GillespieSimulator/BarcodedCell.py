""" :module BarcodedCell: This module hosts the BarcodedCell class. """

from GillespieSimulator.Agent import Agent


class BarcodedCell(Agent):
    """Class that implements an organism that is tagged by a genomic barcode."""

    def __init__(self,
                 cell_id=None,
                 mutation_id=None,
                 parent_mutation_ids=None,
                 date_of_birth=None,
                 number_of_copies=None,
                 division_rate=None,
                 death_rate=None,
                 genome_size=None,
                 barcode_size=None,
                 ):

        """ BarcodedCell constructor.

        :param genome_size: The size (basepairs) of the organism's genome.
        :type  genome_size: int

        :param barcode_size: The length (nucleotides) of the barcode sequence.
        :type  barcode_size: int

        """

        # Init parent class.
        super(BarcodedCell, self).__init__(cell_id,
                                           mutation_id,
                                           parent_mutation_ids,
                                           date_of_birth,
                                           number_of_copies,
                                           division_rate,
                                           death_rate)

        # Init public members.
        self.genome_size = genome_size
        self.barcode_size = barcode_size

        # Init private attributes.

        # End __init__

    @property
    def genome_size(self):
        return self.__genome_size

    @genome_size.setter
    def genome_size(self, value):
        if value is None:
            value = 1

        self.__genome_size = value

    @property
    def barcode_size(self):
        """ The length of the barcode sequence. """

        return self.__barcode_size

    @barcode_size.setter
    def barcode_size(self, val):
        if val is None:
            val = 0
        self.__barcode_size = val
