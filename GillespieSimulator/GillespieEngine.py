""" :module GillespieEngine: Module to host the GillespieEngine class."""

# Imports
import numpy
import sys
import tempfile
import os
import codecs
import json
from GillespieSimulator.BarcodedCell import BarcodedCell
from GillespieSimulator.Parameters import Parameters

import logging

logging.basicConfig(format='%(asctime)s %(levelname)s:%(message)s', level=logging.WARNING)


class GillespieEngine(object):
    """ :class GillespieEngine: Class that implements the Gillespie stochastic simulation algorithm. """

    def __init__(self,
                 parameters=None,
                 population=None,
                 outdir=None,
                 restart_snapshot=None,
                 ):
        """ Constructor of the GillespieEngine class.

        :param population: The initial population, defaults to [1].
        :type  population: list

        :param parameters: The simulation parameters, defaults to GillespieSimulator.Parameters.Parameters().
        :type  parameters: GillespieSimulator.Parameters.Parameters

        :param outdir: The directory where to write simulation data. Default is to use a newly created temporary directory the location of which depends on system settings. On many LINUXes, the outdir will be created under /tmp. Check the $TEMP variable if uncertain.
        :type  outdir: str

        :param restart_snapshot: If given, the code attempts to read a the named file and to continue the simulation.
        :type  restart_snapshot: str
        """

        # Public interface.
        self.__population = None
        self.population = population
        self.parameters = parameters
        self.outdir = outdir
        self.restart_snapshot = restart_snapshot

        # Initialize private attributes.
        # The competition matrix:
        # A_ij*dt = number of events during infinitesimal time interval dt,
        # in which a cell of type i competes with a cell of type j,
        # the j-cell survives while the i-cell dies.
        cm = numpy.random.normal(self.parameters.pdf_mean,
                                                                  self.parameters.pdf_rms,
                                                                  (len(self.population),
                                                                   len(self.population)
                                                                   )
                                                                  )
        cm = numpy.clip(cm, 0., cm.max())
        # numpy.fill_diagonal(cm, 1.0)
        
        self.__competition_matrix = cm
        
        
            
        logging.debug("Initial competition matrix=%s", str(self.__competition_matrix))
        # The constrained reaction rates:
        # a_i^mu*dt is the number of events during the infinitesimal time interval dt,
        # in which any cell of type i undergoes a reaction of type mu given that there are n_i cells of type i.
        self.__constrained_reaction_rates = numpy.empty(shape=(len(self.population), 4))

        # The reaction sum
        # a_0 is the sum over all constrained reaction rates and serves as a normalization factor for
        # the probability distribution functions.
        self.__reaction_sum = 0

        # Create the cell population.
        self.__cells = [BarcodedCell(cell_id=i,
                                     mutation_id=0,
                                     date_of_birth=0,
                                     parent_mutation_ids=[],
                                     number_of_copies=p,
                                     division_rate=self.parameters.division_rate,
                                     death_rate=self.parameters.death_rate,
                                     barcode_size=self.parameters.barcode_size,
                                     genome_size=self.parameters.genome_size,
                                     ) for i, p in enumerate(self.population)
                        ]

        logging.debug("Engine created.")
        logging.debug("Population=%s", str(self.population))

        # Init the snapshots dict.
        self.__snapshots = {}

    @property
    def population(self):
        """ The current population as a list of values indicating the number of individuals that share the same cell_id AND mutation_id.

        :raises RuntimeError: Population already set.

        """
        return self.__population

    @population.setter
    def population(self, val):
        if val is None:
            val = [1]
        if not hasattr(val, "__iter__"):
            raise TypeError("Population must be iterable.")
        if not all([isinstance(v, int) for v in val]):
            raise TypeError("Population must be an iterable over non-negative integers, e.g. [1,1,0].")

        self.__population = val

    @property
    def parameters(self):
        """ The simulation parameters. """
        return self.__parameters

    @parameters.setter
    def parameters(self, val):
        if val is None:
            val = Parameters()
        self.__parameters = val

    @property
    def outdir(self):
        """ The directory where simulation data is written."""
        return self.__outdir

    @outdir.setter
    def outdir(self, val):
        if val is None:
            val = tempfile.mkdtemp(prefix='SSA')

        elif not isinstance(val, str):
            raise AttributeError("The parameter 'outdir' must be a path to a non-existing directory.")
        elif not os.path.exists(val):
            os.makedirs(val)

        self.__outdir = val

    @property
    def restart_snapshot(self):
        """ The directory where simulation data is written."""
        return self.__restart_snapshot

    @restart_snapshot.setter
    def restart_snapshot(self, val):
        if val is None:
            return

        elif not isinstance(val, str):
            raise AttributeError("The parameter 'restart_snapshot' must be a path.")
        elif not os.path.exists(val):
            raise IOError("Restart snapshot {} does not exist.".format(val))

        self.__restart_snapshot = val
        self.load_snapshot(self.__restart_snapshot)

    def load_snapshot(self, restart_snapshot):
        """ Load snapshots from given directory. """

        with codecs.open(restart_snapshot, 'r', encoding='utf-8') as fp:
            snap = json.load(fp)['data']

        self.population = snap['data']['population']
        self.__competition_matrix = numpy.array(
            snap['data']['competition_matrix']).reshape(
                len(self.population),len(self.population)
                )

        self.__cells = [BarcodedCell(
            cell_id=c['cell_id'],
            mutation_id=c['mutation_id'],
            parent_mutation_ids=c['parent_mutation_ids'],
            date_of_birth=c['date_of_birth'],
            number_of_copies=c['number_of_copies'],
            division_rate=c['division_rate'],
            death_rate=c['death_rate']) for c in snap['data']['cells']]

    def _calculate_conditional_reaction_rates(self):
        """ Calculate the conditional reaction rates for the current conditions. """

        self.__reproduction_rates = None

    def perform_iteration(self, time, save_snapshot=True):
        """ Perform a single iteration of the Gillespie algorithm."""
        return self._perform_iteration(time, save_snapshot)

    def _perform_iteration(self, time, save_snapshot=True):
        """"""
        """ Implementation of the Gillespie algorithm
        :private:
        """

        logging.debug("Starting iteration at simulated time %f", time)

        total_population_size = sum(self.population)
        if self.parameters.carrying_capacity is not None:
            N_over_K = total_population_size/self.parameters.carrying_capacity
        else:
            N_over_K = 0.0

        conditional_reproduction = numpy.array(
            [agent.number_of_copies * effective_division_rate(agent.division_rate,
                                                              self.parameters.theta,
                                                              N_over_K
                                                             ) * (1. - self.parameters.mutation_probability)
             for agent in self.__cells]
        )
        
        conditional_mutation = numpy.array(
            [agent.number_of_copies * effective_division_rate(agent.division_rate,
                                                              self.parameters.theta,
                                                              N_over_K
                                                             ) * self.parameters.mutation_probability 
             for agent in self.__cells]
        )
        
        conditional_death = numpy.array(
            [agent.number_of_copies * effective_death_rate(agent.death_rate,
                                                           self.parameters.theta,
                                                           N_over_K)
             for agent in self.__cells]
        )

        number_of_cell_types = len(self.__cells)

        # Competition
        copies = numpy.array([agent.number_of_copies for agent in self.__cells])
        weights = numpy.outer(copies, copies)

        logging.debug("n_i*n_j = \n %s", str(weights))

        diagonal = 0.5 * numpy.diag(copies * (copies + 1.))

        logging.debug("1/2*n_i*(n_i-1)) = \n %s", str(diagonal))

        weights = weights - diagonal

        logging.debug("weights =\n %s", str(weights))

        conditional_competition = self.__competition_matrix * weights

        logging.debug("Conditional competition matrix\n %s", str(conditional_competition))

        # Make one rate matrix of n_mutants cols and n_reaction rows.
        conditional_rates = numpy.vstack((conditional_reproduction,
                                          conditional_mutation,
                                          conditional_death,
                                          conditional_competition.sum(axis=1)  # Sum over all competition partners.
                                          )
                                         )
        logging.debug("conditional_rates\n %s", str(conditional_rates))

        conditional_rates = conditional_rates.flatten()
        logging.debug("Flat conditional_rates\n %s", str(conditional_rates))
        conditional_sum = conditional_rates.sum()
        logging.debug("conditional_sum\n %s", str(conditional_sum))

        # Gillespie algorithm.
        # Draw two random numbers between 0 and 1.
        r1, r2 = numpy.random.random(2)
        logging.debug("r1=%f\tr2=%f", r1, r2)
        
        if save_snapshot:
            logging.info("Saving snapshot at t = %f.", time)
            # Get a snapshot of the current state.
            snapshot = {'data': self._simulation_snapshot(
                time,
                r1,
                r2,
                conditional_rates,
            )}
            self.save_snapshot(snapshot)

        # Calculate the time of next event.
        tau = numpy.log(1. / r1) / conditional_sum
        time += tau

        logging.debug("tau=%f", tau)

        cutoff = r2 * conditional_sum
        logging.debug("cutoff=r2*conditional_sum=%f", cutoff)

        # Calculate the index of the next reaction.
        mu = 0
        partial_sum = conditional_rates[mu]

        while partial_sum < cutoff:
            mu += 1
            partial_sum += conditional_rates[mu]
            logging.debug("mu=%d\tpartial_sum=%f", mu, partial_sum)
        logging.debug("mu=%d\tpartial_sum=%f", mu, partial_sum)

        # Reconstruct the reaction event.
        reaction_index = mu // number_of_cell_types
        celltype_index = mu % number_of_cell_types

        logging.debug("reaction_idx=%d\tcelltype_idx=%d", reaction_index, celltype_index)

        agent = self.__cells[celltype_index]

        # Case division:
        if reaction_index == 0:
            agent.divide()

        # Case mutation.
        elif reaction_index == 1:

            # Draw a new pair of division and death rates, normally distributed parental type rates.
            if self.parameters.division_rate_fwhm != 0.0:
                rep = max(0.0, numpy.random.normal(agent.division_rate, self.parameters.division_rate_fwhm/0.425))
            else:
                rep = self.parameters.division_rate
                
            if self.parameters.death_rate_fwhm != 0.0:
                d = max(0.0, numpy.random.normal(agent.death_rate, self.parameters.death_rate_fwhm))
            else:
                d = agent.death_rate
                
            logging.debug("rep = %f", rep)
            logging.debug("d   = %f", d)

            # Get the mutation index (= mutation index of latest cell type added.)
            mutation = self.__cells[-1].mutation_id + 1

            mutant = agent.mutate(date=time,
                                  mutation=mutation,
                                  division_rate=rep,
                                  death_rate=d,
                                  )

            # Decide if mutation occurs inside the barcoded region.
            is_barcode_mutation = False
            random_number = numpy.random.random()
            probability_of_barcode_mutation = agent.barcode_size / agent.genome_size

            if random_number <= probability_of_barcode_mutation:
                is_barcode_mutation = True
                logging.debug("Barcode mutation.")
                # Mutation is in the barcode.
                # This yields a new type with properties identical to parent.
                # Can later on be identified by filtering on different cell_id but same mutation_id.

                mutant = agent.mutate(date=time,
                                      mutation=agent.mutation_id,
                                      division_rate=agent.division_rate,
                                      death_rate=agent.death_rate,
                                      )
                mutant.cell_id = self.__cells[-1].cell_id + 1

            # Update competition matrix.
            competition_vector_mutant_vs_other = numpy.random.normal(
                loc=self.__competition_matrix[agent.cell_id, :],
                scale=self.parameters.pdf_rms,
            )

            competition_vector_other_vs_mutant = numpy.random.normal(
                loc=self.__competition_matrix[:, agent.cell_id],
                scale=self.parameters.pdf_rms,
            )

            new_competition_matrix = numpy.empty(shape=(
                len(self.__cells) + 1,
                len(self.__cells) + 1,
                )
            )
            # If barcode mutation, copy competition values from parent.
            if is_barcode_mutation:
                competition_vector_mutant_vs_other = self.__competition_matrix[agent.cell_id, :]
                competition_vector_other_vs_mutant = self.__competition_matrix[:, agent.cell_id]

            # Fill in the old and new values.
            new_competition_matrix[:-1, :-1] = self.__competition_matrix
            new_competition_matrix[-1, :-1] = competition_vector_mutant_vs_other
            new_competition_matrix[:-1, -1] = competition_vector_other_vs_mutant
            # new_competition_matrix[-1, -1] = 0.0 
            # new_competition_matrix[-1, -1] = self.parameters.pdf_mean 
            new_competition_matrix[-1, -1] = numpy.random.normal(loc=self.__competition_matrix[agent.cell_id, agent.cell_id], scale=self.parameters.pdf_rms) 
            
            if is_barcode_mutation:
                new_competition_matrix[-1, -1] = self.__competition_matrix[agent.cell_id, agent.cell_id]

            self.__competition_matrix = numpy.abs(new_competition_matrix)

            logging.debug("New competition matrix=%s", str(self.__competition_matrix))
            self.__cells.append(mutant)

        # Case death by natural cause or competition.
        elif reaction_index == 2:
            agent.die()

        else:
            logging.debug("Death by competition")

            agent.die()

        if all([agent.number_of_copies == 0 for agent in self.__cells]):
            sys.exit("No more cells alive, simulation aborted.")

        self.__population = [agent.number_of_copies for agent in self.__cells]
        logging.debug("Population: %s", self.population)

        
        return time

    def _simulation_snapshot(self,
                             *args,
                             ):
        """ Assemble the current state into a json branch """

        snap = dict()
        snap['parameters'] = {}
        snap['parameters']['time'] = args[0]
        snap['random'] = {}
        snap['random']['r1'] = args[1]
        snap['random']['r2'] = args[2]

        snap['data'] = {}
        snap['data']['conditional_rates'] = args[3].tolist()
        snap['data']['competition_matrix'] = self.__competition_matrix.tolist()
        snap['data']['population'] = self.population
        snap['data']['cells'] = [{
            'cell_id': c.cell_id,
            'mutation_id': c.mutation_id,
            'parent_mutation_ids': c.parent_mutation_ids,
            'date_of_birth': c.date_of_birth,
            'number_of_copies': c.number_of_copies,
            'division_rate': c.division_rate,
            'death_rate': c.death_rate,
        } for c in self.__cells]

        return snap

    def save_snapshot(self, snapshot):
        """ Write (append) the  snapshots to a .json in the configured simulation
         output directory. """

        with codecs.open(os.path.join(self.outdir, "snp_{0:8.7e}.json".format(snapshot['data']["parameters"]["time"])), 'w', encoding='utf-8') as fp:
            json.dump(snapshot, fp, separators=(',', ':'))
    
    def run(self, snapshot_interval=1.0):
        """ Run the simulation until max time. """
        
        # Start a few iterations.
        time = 0.0
        snapshot_time = time
        save_next_snp = True
        while time < self.parameters.max_time:
            time = self.perform_iteration(time=time,
                                            save_snapshot=save_next_snp
                                            )
            logging.debug("\n\n%8.7e\n\n", time)
            save_next_snp = time > snapshot_time + snapshot_interval
            if save_next_snp:
                snapshot_time = time

        # Save the last snapshot.
        time = self.perform_iteration(time=time, save_snapshot=True)


""" # Open json file.
        snapshots = [f for f in os.listdir(self.restart_snapshot) if f.split('.')[-1] == 'json']
        times = [float(".".join(f.split('.')[:-1]).split('_')[-1]) for f in snapshots]
        latest_time = max(times)
        latest_snapshot = 'snp_{0:8.7e}.json'.format(latest_time)

        latest_snapshot_file = os.path.join(self.restart_snapshot, latest_snapshot)
        """

def effective_death_rate(d, theta, NK):
    return d - (d-theta)*NK

def effective_division_rate(b, theta, NK):
    return b - (b-theta)*NK
