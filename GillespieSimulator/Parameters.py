""" :module Parameters:
    Module to host the parameter class for a Gillespie simulation
"""

import numpy
import logging


class Parameters(object):
    """ :class Parameters:
        Class to manage the parameters of a Gillespie simulation
    """

    def __init__(self,
                 max_time=None,
                 division_rate=None,
                 division_rate_fwhm=None,
                 mutation_probability=None,
                 mutation_probability_fwhm=None,
                 death_rate=None,
                 death_rate_fwhm=None,
                 pdf_mean=None,
                 pdf_rms=None,
                 barcode_size=None,
                 genome_size=None,
                 theta=None,
                 carrying_capacity=None,
                 ):
        """ Construct the simulation parameters

        :param max_time: The maximum time step to simulate. Default 1
        :type  max_time: int

        :param division_rate:
            The rate at which cells divide (A -> A+A). Default 1.0
        :type  division_rate: float

        :param mutation_probability:
            The probability at which a mutation occurs during a cell divisions.
            Default: 0.0
        :type  mutation_probability: float

        :param death_rate: The rate at which cells die. Default 0.0
        :type  death_rate: float

        :param pdf_mean:
            The center of the Gaussian probability distribution function
            from which to draw the elements of the competition matrix.
            Default 0.0
        :type  pdf_mean: float

        :param pdf_rms:
            The standard deviation of the Gaussian probability distribution
            function from which to draw the competition matrix and
            (if not specified) the division and death rates of mutants.
            For the latter, the distribution will be centered on the
            configured division_rate and death_rate, respectively. Default 0.0
        :type  pdf_rms:  float

        :param genome_size: The size of the cell's genome
        :type  genome_size: int

        :param barcode_size: The size of the barcode sequence.
        :type  barcode_size: int

        :param theta: The growth factor for carrying capacity (default 1).
        :type  theta: float

        :param carrying_capacity:
            The carrrying capacity of the population (environment).
            Default: None (infinite capacity).
        :type  carrying_capacity: float or None

        :param mutation_probability_fwhm:
            The full width at half maximum of the distribution of mutation
            probabilities.  If 0, all agents have the same mutation
            probability. If None, value will be read from pdf_rms. Default: None
        :type  mutation_probability_fwhm: float

        :param death_rate_fwhm:
            The full width at half maximum of the death rate. If 0, all agents
            have the same death rate. If None, value is read from `pdf_rms`.
            Default : None
        :type  death_rate_fwhm: float

        :param division_rate_fwhm:
            The full width at half maximum of the division rate.
            If 0, all agents have the same death rate.
            If None, value is read from `pdf_rms`.
            Default : None
        :type  division_rate_fwhm: float

        """

        self.max_time = max_time
        self.division_rate = division_rate
        self.mutation_probability = mutation_probability
        self.death_rate = death_rate
        self.pdf_mean = pdf_mean
        self.pdf_rms = pdf_rms
        self.genome_size = genome_size
        self.barcode_size = barcode_size
        self.theta = theta
        self.carrying_capacity = carrying_capacity
        self.division_rate_fwhm = division_rate_fwhm
        self.death_rate_fwhm = death_rate_fwhm
        self.mutation_probability_fwhm = mutation_probability_fwhm

    @property
    def mutation_probability_fwhm(self):
        """ The FWHM of the distribution of mutation probabilities. """
        return self.__mutation_probability_fwhm
    @mutation_probability_fwhm.setter
    def mutation_probability_fwhm(self, val):
        # Type and value checks.
        if val is None:
            val = self.pdf_rms/0.425 # Factor 0.425 to convert from rms to fwhm.
            logging.info("Setting mutation_probability_fwhm to {} according to \
                         'pdf_rms'.", val)

        if not isinstance(val, (float, int)):
            raise TypeError(
                "Wrong type: expected float, received {}.".format(
                type(val)
                )
            )
        if val < 0.0:
            raise ValueError("Found non-negative value ({}) \
                             for mutation_probability_fwhm.".format(val))
        self.__mutation_probability_fwhm = val

    @property
    def death_rate_fwhm(self):
        """ The FWHM of the distribution of death rates. """
        return self.__death_rate_fwhm
    @death_rate_fwhm.setter
    def death_rate_fwhm(self, val):
        # Type and value checks.
        if val is None:
            val = self.pdf_rms/0.425 # Factor 0.425 to convert from rms to fwhm.
            logging.info("Setting death_rate_fwhm to {} according to \
                         'pdf_rms'.", val)

        if not isinstance(val, (float, int)):
            raise TypeError(
                "Wrong type: expected float, received {}.".format(
                type(val)
                )
            )
        if val < 0.0:
            raise ValueError("Found non-negative value ({}) \
                             for death_rate_fwhm.".format(val))
        self.__death_rate_fwhm = val

    @property
    def division_rate_fwhm(self):
        """ The FWHM of the distribution of division rates. """
        return self.__division_rate_fwhm
    @division_rate_fwhm.setter
    def division_rate_fwhm(self, val):
        # Type and value checks.
        if val is None:
            # Factor 0.425 to convert from rms to fwhm.
            val = self.pdf_rms/0.425
            logging.info("Setting division_rate_fwhm to {} according to \
                         'pdf_rms'.", val)

        if not isinstance(val, (float, int)):
            raise TypeError(
                "Wrong type: expected float, received {}.".format(
                    type(val)
                )
            )
        if val < 0.0:
            raise ValueError("Found non-negative value ({}) \
                             for division_rate_fwhm.".format(val))
        self.__division_rate_fwhm = val

    @property
    def max_time(self):
        '''The maximum time step to simulate.'''
        return self.__max_time
    @max_time.setter
    def max_time(self, val):
        if val is None:
            val = 1
        self.__max_time = val

    @property
    def division_rate(self):
        '''The rate at which cells divide.'''
        return self.__division_rate
    @division_rate.setter
    def division_rate(self, val):
        if val is None:
            val = 1.0
        self.__division_rate = val

    @property
    def mutation_probability(self):
        '''The rate at which cell divisions with mutations occur.'''
        return self.__mutation_probability
    @mutation_probability.setter
    def mutation_probability(self, val):
        if val is None:
            val = 0.0
        self.__mutation_probability = val

    @property
    def death_rate(self):
        '''The rate at which cells die.'''
        return self.__death_rate
    @death_rate.setter
    def death_rate(self, val):
        if val is None:
            val = 0.0
        self.__death_rate = val

    @property
    def pdf_mean(self):
        '''The center of the Gaussian probability distribution function from which to draw the elements of the competition matrix.'''
        return self.__pdf_mean
    @pdf_mean.setter
    def pdf_mean(self, val):
        if val is None:
            val = 0.0
        self.__pdf_mean = val

    @property
    def pdf_rms(self):
        '''The standard deviation of the Gaussian probability distribution function from which to draw the competition matrix and the division and death rates of mutants.'''
        return self.__pdf_rms
    @pdf_rms.setter
    def pdf_rms(self, val):
        if val is None:
            val = 0.0
        self.__pdf_rms = val

    @property
    def barcode_size(self):
        """ The size of the barcode sequence. """

        return self.__barcode_size
    @barcode_size.setter
    def barcode_size(self, val):
        if val is None:
            val = 0
        self.__barcode_size = val

    @property
    def genome_size(self):
        """ The size of the entire genome. """

        return self.__genome_size
    @genome_size.setter
    def genome_size(self, val):
        if val is None:
            val = 1
        self.__genome_size = val

    @property
    def theta(self):
        """ The intrinsic growth rate (???). """

        return self.__theta
    @theta.setter
    def theta(self, val):
        if val is None:
            val = 0.0
        self.__theta = val

    @property
    def carrying_capacity(self):
        """ The carrrying capacity of the environment."""

        return self.__carrying_capacity

    @carrying_capacity.setter
    def carrying_capacity(self, val):
        self.__carrying_capacity = val



