import os
import sys
import numpy
import json
import codecs
import pandas
#from matplotlib import pyplot
import ipywidgets as ipyw
import logging
from matplotlib import pyplot as plt

logging.getLogger().setLevel(logging.WARNING)

def load_snapshots(run_dir):
    snapshots = dict()

    snapshot_files = [os.path.join(run_dir, f) for f in os.listdir(run_dir) if f.endswith(".json") ]
    
    snapshot_files.sort()

    for snap in snapshot_files:
        with codecs.open(snap, 'r',  encoding='utf-8') as fp:
            data = json.load(fp)['data']
            time = data['parameters']['time']
            snapshots[time] = data

    return snapshots

def load_competition_matrix(snapshot):
    raw_cm =  numpy.array(snapshot['data']["competition_matrix"])

    return raw_cm

def sorted_competition_matrix(snapshot):

    raw_cm = load_competition_matrix(snapshot)

    cells = snapshot['data']['cells']

    cell_ids = [cell['cell_id'] for cell in cells]
    mutn_ids = [cell['mutation_id'] for cell in cells]

    keys_to_sort_after = ["{}.{}".format(c, m) for c, m in zip(cell_ids, mutn_ids)]
    # Sort according to cell id.
    # Iterate over column.

    # Basically, the competition matrix is ordered by mutation ids but we need
    # it to be sorted by cell ids (barcode). So, have to create an indexed array
    # with index being the cell id and then sort.
    rows = raw_cm[:]

    items = zip(keys_to_sort_after, rows)

    sorted_items = sorted(items)
    row_sorted_cm = numpy.array([s[1] for s in sorted_items])

    # Sort rows.
    columns = row_sorted_cm.T[:]
    items = zip(keys_to_sort_after, columns)
    sorted_items = sorted(items)
    sorted_cm = numpy.array([s[1] for s in sorted_items])

    return sorted_cm.T

def reduced_competition_matrix(snapshot):
    comp = load_competition_matrix(snapshot)

    data = snapshot["data"]
    cells = data["cells"]
    number_of_cells = len(cells)

    number_of_lineages = len(set([c['cell_id'] for c in cells]))

    reduced_comp = numpy.empty(shape=(number_of_lineages, number_of_lineages))

    # The braindead way ...
    mutant_indices = [(i,j) for i in range(number_of_cells) for j in range(number_of_cells)]
    for l1 in range(number_of_lineages):
        for l2 in range(number_of_lineages):

            # Find all elements in the competition matrix that link two mutants
            # with the current l1 and l2 cell ids.
            competition_matrix_elements = [comp[i,j] for i,j in mutant_indices
                                           if (cells[i]['cell_id'] == l1 and cells[j]['cell_id'] == l2)]

            reduced_comp[l1, l2] = max(competition_matrix_elements)
            # reduced_comp[l1, l2] = numpy.mean(competition_matrix_elements)

    return reduced_comp

def death_rate(snapshots):
    """ Get the death rate of each mutant. """
    
    ts = list(snapshots.keys())
    ts.sort()

    cell_mutant_ids = [(c['cell_id'], c['mutation_id']) for c in snapshots[ts[-1]]['data']['cells'] ]
    
    columns = pandas.MultiIndex.from_tuples(cell_mutant_ids, names=['Barcode ID', 'Mutant ID'])
    index = pandas.Index([float(t) for t in ts], name='time (generations)')
    ret = pandas.DataFrame(index=index, columns=columns)

    for t in ts:
        ret.loc[t] = dict([((c['cell_id'],c['mutation_id']),c['death_rate']) for c in snapshots[t]['data']['cells']])
    
    return ret
 
def division_rate(snapshots):
    """ Geth the birth rate per mutant."""
    
    ts = list(snapshots.keys())
    ts.sort()

    cell_mutant_ids = [(c['cell_id'], c['mutation_id']) for c in snapshots[ts[-1]]['data']['cells'] ]
    
    columns = pandas.MultiIndex.from_tuples(cell_mutant_ids, names=['Barcode ID', 'Mutant ID'])
    index = pandas.Index([float(t) for t in ts], name='time (generations)')
    ret = pandas.DataFrame(index=index, columns=columns)

    for t in ts:
        ret.loc[t] = dict([((c['cell_id'],c['mutation_id']),c['division_rate']) for c in snapshots[t]['data']['cells']])
    
    return ret
   

def mutants(snapshots):
    """ Aggregate the frequency of each mutant line and return as pandas.DataFrame where the index is the time and the columns are labeled by cell id and mutation id."""
    
    ts = list(snapshots.keys())
    ts.sort()

    cell_mutant_ids = [(c['cell_id'], c['mutation_id']) for c in snapshots[ts[-1]]['data']['cells'] ]
    
    columns = pandas.MultiIndex.from_tuples(cell_mutant_ids, names=['Barcode ID', 'Mutant ID'])
    index = pandas.Index([float(t) for t in ts], name='time (generations)')
    ret = pandas.DataFrame(index=index, columns=columns)

    for t in ts:
        ret.loc[t] = dict([((c['cell_id'],c['mutation_id']),c['number_of_copies']) for c in snapshots[t]['data']['cells']])
    
    return ret

def load_runs(dirs):
    """ Load all runs from the given list of directories"""

    return dict([(i, load_snapshots(directory)) for i, directory in enumerate(dirs)])

def sort(unsorted_list):
    a=unsorted_list
    a.sort()

    return a

def minmax(a):
    return (min(a), max(a))

def interactive_plot_widget(runs):

    cm_minmax_widget = ipyw.FloatRangeSlider(
        value=[0.0, 1.0],
        min=0,
        max=1.0,
        step=0.001,
        description='Range',
        disabled=False,
        continuous_update=False,
        orientation='horizontal',
        readout=True,
        readout_format='.4f',
    )

    number_of_runs = len(list(runs.keys()))
    run_select_widget = ipyw.IntText(min=0,
                                     max=number_of_runs-1,
                                     description="Run #",
                                     )

    snp_times = list(runs[run_select_widget.value].keys())
    snp_times.sort()

    snapshot_select_widget = ipyw.SelectionSlider(
        options=snp_times,
        value=snp_times[0],
        description='snapshot time',
        disabled=False,
        continuous_update=True,
        orientation='horizontal',
        readout=True
    )

    def handle_run_change(change):
        snp_times = list(runs[run_select_widget.value].keys())
        snp_times.sort()
        snapshot_select_widget.options = snp_times
        snapshot_select_widget.value = snp_times[0]

        cm_max = max([numpy.amax(v['data']['competition_matrix']) for v in runs[run_select_widget.value].values()])
        cm_min = min([numpy.amin(v['data']['competition_matrix']) for v in runs[run_select_widget.value].values()])
        cm_minmax_widget.min = cm_min
        cm_minmax_widget.max = cm_max
        
    run_select_widget.observe(handle_run_change, names='value')

    def plot_cell_counts(run, time):

        fig, axes = plt.subplots(nrows=3, ncols=1, figsize=(10, 15))

        muts = mutants(runs[run])
        grouped = muts.groupby(axis=1, level=0)
        counts = grouped.sum()
        mutations = grouped.count()
       
        colors = ['tab:blue',
                  'tab:orange',
                  'tab:green',
                  'tab:red',
                  'tab:purple',
                  'tab:brown',
                  'tab:pink',
                  'tab:gray',
                  'tab:olive',
                  'tab:cyan'
                 ]
        
        for mut in muts.columns:
            axes[0].semilogy(muts[mut],
                             "o-",
                             color=colors[mut[0] % len(colors)]
                 )
        axes[0].set_title("# mutant cells vs. time")
        
        counts.plot(logy=True,
                    style="o-",
                    title="# barcoded cells vs. time",
                    ax=axes[1]
                   )
        mutations.plot(style="o-",
                       title="# mutations vs. time",
                       ax=axes[2],
                       legend=False,
                       logy=True,
                      )

        axes[1].legend(bbox_to_anchor=(1.05, 1),
                       loc=2,
                       borderaxespad=0.,
                       title="Barcode"
                       )
        axes[0].axvline(x=time)
        axes[1].axvline(x=time)
        axes[2].axvline(x=time)

        return axes

    cell_count_plot_widget = ipyw.interactive_output(plot_cell_counts,
                                                     {'run': run_select_widget,
                                                      'time':snapshot_select_widget
                                                     }
                                                     )

    def plot_reduced_competition_matrix(run, snapshot, cm_minmax):
        rcm = reduced_competition_matrix(
            runs[run][snapshot]
        )

        fig, ax = plt.subplots()
        im, cb = ax.matshow(rcm, cmap='RdBu_r', vmin=cm_minmax[0], vmax=cm_minmax[1]),0# ax.figure.colorbar()
        ax.set_xticks(range(rcm.shape[0]))
        ax.set_yticks(range(rcm.shape[1]))
        
        ax.set_xticklabels([str(i) for i in range(rcm.shape[0])])
        ax.set_yticklabels([str(i) for i in range(rcm.shape[1])])
        

    reduced_competition_matrix_widget = ipyw.interactive_output(
        plot_reduced_competition_matrix, {"run": run_select_widget,
                                          "snapshot": snapshot_select_widget,
                                          "cm_minmax": cm_minmax_widget,
                                          }
    )

    def plot_competition_matrix(run, snapshot, cm_minmax):
        cm = sorted_competition_matrix(
            runs[run][snapshot]
        )

        fig, ax = plt.subplots()
        im, cb = ax.matshow(cm, cmap='RdBu_r', vmin=cm_minmax[0], vmax=cm_minmax[1]),0 #ax.figure.colorbar()
        ax.set_xticks(range(cm.shape[0]))
        ax.set_yticks(range(cm.shape[1]))
        
        ax.set_xticklabels([str(i) for i in range(cm.shape[0])])
        ax.set_yticklabels([str(i) for i in range(cm.shape[1])])

    competition_matrix_widget = ipyw.interactive_output(
        plot_competition_matrix, {"run": run_select_widget,
                                          "snapshot": snapshot_select_widget,
                                          "cm_minmax": cm_minmax_widget,
                                          }
    )

    ret = ipyw.HBox([
        ipyw.VBox(
            [run_select_widget,
             cell_count_plot_widget]
        ),
        ipyw.VBox(
            [snapshot_select_widget,
             ipyw.VBox([
                 ipyw.Text(value=r"Competition matrix: $C_{ij}$"),
                 competition_matrix_widget,
                 reduced_competition_matrix_widget,
                 cm_minmax_widget,
             ]
             )
             ]
        )
    ])
    
    handle_run_change(run_select_widget) 
    return ret
