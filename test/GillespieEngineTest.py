""" :module GillespieEngineTest: Module to host the test class for the GillespieEngine. """

import codecs
import json
import logging
import multiprocessing
import numpy
import os
import shutil
import unittest

from GillespieSimulator.GillespieEngine import GillespieEngine, effective_division_rate, effective_death_rate
from GillespieSimulator.Parameters import Parameters

logging.getLogger().setLevel(level=logging.WARNING)


class GillespieEngineTest(unittest.TestCase):
    """ :class: Test class for the GillespieEngineTest class. """

    @classmethod
    def setUpClass(cls):
        """ Setup the test class. """
        pass

    @classmethod
    def tearDownClass(cls):
        """ Tear down the test class. """
        pass

    def setUp(self):
        """ Setup the test instance. """
        self.__dirs_to_remove = []
        self.__files_to_remove = []

    def tearDown(self):
        """ Tear down the test instance. """
        for f in self.__files_to_remove:
            if os.isfile(f):
                os.remove(f)

        for d in self.__dirs_to_remove:
            if os.path.isdir(d):
                shutil.rmtree(d)

    def testConstruction(self):
        """ Test the construction of a GillespieEngineTest instance. """

        # Setup the instance.
        instance = GillespieEngine()

        # Check it's instance.
        self.assertIsInstance(instance, GillespieEngine)

    def testDefaultAttributes(self):
        """ Check that default attributes are correctly set. """
        engine = GillespieEngine()

        self.assertEqual(engine.population, [1])

    def testIO(self):
        """ Test the IO functionality."""

        engine = GillespieEngine()

        seed = 1089708774
        numpy.random.seed(seed)
        time = engine._perform_iteration(time=0.0, save_snapshot=True)

        fp = codecs.open(os.path.join(engine.outdir,
                                      "snp_{0:8.7e}.json".format(0.0),
                                      ),
                         'r',
                         encoding='utf-8'
                         )
        snapshot = json.loads(fp.read())

        fp.close()

    def testSingleIteration(self):
        """ Test that performing a single iteration works correctly."""

        engine = GillespieEngine()

        seed = 1089708774
        numpy.random.seed(seed)

        time = engine._perform_iteration(time=0.0)

    def testIterationsNonDefaultParameters(self):
        """ Test that performing a single iteration works correctly with non-default parameters."""

        parameters = Parameters(max_time=10,
                                division_rate=1.0,
                                mutation_probability=0.0,
                                death_rate=0.2,
                                pdf_mean=0.0,
                                pdf_rms=.1,
                                theta=0.0,
                                carrying_capacity=200,
                                )

        seed = 1089708774
        numpy.random.seed(seed)

        engine = GillespieEngine(parameters=parameters)

        engine.run(snapshot_interval=1)


    def testSeeded(self):
        """ Test against a benchmark with seeded rng."""

        parameters = Parameters(max_time=10,
                                division_rate=1.0,
                                mutation_probability=0.3,
                                death_rate=0.02,
                                pdf_mean=0.3,
                                pdf_rms=.1,
                                barcode_size=0,
                                genome_size=1,
                                )
        seed = 108978774
        numpy.random.seed(seed)

        engine = GillespieEngine(parameters=parameters, population=[1, 1, 1])

        engine.run(snapshot_interval=1)

        self.assertEqual(engine.population, [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3, 0, 2, 2, 1])
        

    def testSeeded3Strains(self):
        """ Test against a benchmark with seeded rng and 3 initial cell types."""

        parameters = Parameters(max_time=10,
                                division_rate=1.0,
                                mutation_probability=0.3,
                                death_rate=0.2,
                                pdf_mean=0.01,
                                pdf_rms=.001,
                                )

        seed = 1089708774
        numpy.random.seed(seed)

        engine = GillespieEngine(parameters=parameters,
                                 population=[1, 1, 1])

        engine.run(snapshot_interval=1)

    def test_zero_competition(self):
        """ Test we can run a simulation with zero competition."""
        parameters = Parameters(max_time=10,
                                division_rate=1.0,
                                mutation_probability=1e-3,
                                death_rate=1e-2,
                                pdf_mean=0.00,
                                pdf_rms=1e-15,
                                )

        seed = 1089708774
        numpy.random.seed(seed)

        engine = GillespieEngine(parameters=parameters,
                                 population=[1, 1, 1])

        time = 0.0
        while time < 10:
            time += engine._perform_iteration(time=time)


    def testIterations(self):
        """ Test performing a number of iterations."""

        engine = GillespieEngine()

        time = 0.0

        while time < 10:
            time = engine._perform_iteration(time=time)


    def testBarcodeMutation(self):
        """ Test that a run with barcode mutations yields expected results."""

        parameters = Parameters(max_time=5,
                                division_rate=1.0,
                                mutation_probability=0.3,
                                death_rate=0.2,
                                pdf_mean=0.01,
                                pdf_rms=.001,
                                barcode_size=5,
                                genome_size=10,
                                )

        seed = 1089708774
        numpy.random.seed(seed)

        engine = GillespieEngine(parameters=parameters,
                                 population=[1, 1, 1])

        time = 0.0
        while time < 10:
            time = engine._perform_iteration(time=time, save_snapshot=True)

    def test_restart(self):
        """ Test that we can restart a simulation from a previous snapshot."""

        parameters = Parameters(max_time=4.0,
                                division_rate=1.0,
                                mutation_probability=0.3,
                                death_rate=0.2,
                                pdf_mean=0.01,
                                pdf_rms=.001,
                                barcode_size=5,
                                genome_size=10,
                                )

        seed = 1089708774
        numpy.random.seed(seed)

        engine = GillespieEngine(parameters=parameters,
                                 population=[1, 1, 1],
                                 outdir='out')

        # Cleanup.
        self.__dirs_to_remove.append(engine.outdir)

        # Start a few iterations.
        time = 0.0
        snapshot_time = time
        save_next_snp = True
        while time < engine.parameters.max_time:
            time = engine.perform_iteration(time=time, save_snapshot=save_next_snp)
            logging.debug("\n\n%8.7e\n\n", time)
            save_next_snp = time > snapshot_time + 1.0
            if save_next_snp:
                snapshot_time = time

        # Save the last snapshot.
        _ = engine.perform_iteration(time=time, save_snapshot=True)

        # New engine with loaded snapshot.
        rerun_engine = GillespieEngine(parameters=parameters, outdir='out', restart_snapshot=os.path.join("out", "snp_{0:8.7e}.json".format(time)))

        # Perform one more iteration with the reloaded snapshot.
        _ = rerun_engine._perform_iteration(time, save_snapshot=True)

        self.assertIn("snp_{0:8.7e}.json".format(time), os.listdir(rerun_engine.outdir))

    def test_run(self):
        """ Test running a simulation with the helper method 'run()'."""
        parameters = Parameters(max_time=4.0,
                                division_rate=1.0,
                                mutation_probability=0.3,
                                death_rate=0.2,
                                pdf_mean=0.01,
                                pdf_rms=.001,
                                barcode_size=5,
                                genome_size=10,
                                )

        seed = 1089708774
        numpy.random.seed(seed)

        engine = GillespieEngine(parameters=parameters,
                                 population=[1, 1, 1],
                                 outdir='out')

        # Cleanup.
        self.__dirs_to_remove.append(engine.outdir)
        
        # Run the simulation.
        engine.run(snapshot_interval=1.0)


    def test_exponential_growth(self):
        """ Test the special case of exponential growth."""

        parameters = Parameters(max_time=5,
                                division_rate=1.0,
                                mutation_probability=0.0,
                                death_rate=0.0,
                                pdf_mean=0.0,
                                pdf_rms=.0,
                                barcode_size=0,
                                genome_size=1,
                                )

        # Setup the simulation engine.

        number_of_runs = 10
        runs = range(number_of_runs)
        cpu_count = multiprocessing.cpu_count()

        logging.debug('number_of_runs = %d', number_of_runs)
        logging.debug('cpu_count = %d', cpu_count)

        pool = multiprocessing.Pool(cpu_count)
        results = [pool.apply_async(func=run_job, args=[run, parameters, [1, 1, 1]]) for run in runs]

        for result in results:
            outdir = result.get()
            self.__dirs_to_remove.append(outdir)

    def test_logistic_growth(self):
        """ Test the special case of logistic growth. """

        parameters = Parameters(max_time=10,
                                division_rate=1.0,
                                mutation_probability=0.0,
                                death_rate=0.1,
                                pdf_mean=0.0,
                                pdf_rms=.0,
                                barcode_size=0,
                                genome_size=1,
                                theta=0.,
                                carrying_capacity=100,
                                )

        number_of_runs = 192
        runs = range(number_of_runs)

        cpu_count = multiprocessing.cpu_count()

        logging.debug('number_of_runs = %d', number_of_runs)
        logging.debug('cpu_count = %d', cpu_count)

        pool = multiprocessing.Pool(cpu_count)
        results = [pool.apply_async(func=run_job, args=[run, parameters, [10, 10, 10]]) for run in runs]
        pool.close()
        pool.join()

    def test_effective_division_rate(self):
        """ Test that 'effective_division_rate' returns the expected values. """
        
        beff = effective_division_rate(1.0, 
                                       theta=0.5,
                                       NK = 0.1)
        
        self.assertAlmostEqual(beff, 0.95, 5, None, None)

    def test_effective_death_rate(self):
        """ Test that 'effective_death_rate' returns the expected values. """
        
        beff = effective_death_rate(1.0, 
                                       theta=0.5,
                                       NK = 0.1)
        
        self.assertAlmostEqual(beff, 0.95, 5, None, None)

def run_job(run, parameters, population):
    numpy.random.seed(run)
    engine = GillespieEngine(parameters=parameters,
                             population=population,
                             outdir='out/out_{0:04d}'.format(run),
                             )
    engine.run(snapshot_interval=1)
    
    return engine.outdir


if __name__ == "__main__":
    unittest.main()
