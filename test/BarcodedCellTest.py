""" :module BarcodedCellTest: Hosts the test class for the BarcodedCell class."""
import unittest

from GillespieSimulator.BarcodedCell import BarcodedCell
from GillespieSimulator.Agent import Agent


class MyTostCase(unittest.TestCase):
    def test_construction(self):
        """ Test the construction of a BarcodedCell instance. """

        barcoded_cell = BarcodedCell()

        self.assertIsInstance(barcoded_cell, BarcodedCell)
        self.assertIsInstance(barcoded_cell, Agent)


if __name__ == '__main__':
    unittest.main()

