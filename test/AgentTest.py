""" :module AgentTest: Module to host the test class for the Agent class. """

import unittest
import sys

from GillespieSimulator.Agent import Agent

class AgentTest(unittest.TestCase):
    """ :class: Test class for the Agent class. """

    @classmethod
    def setUpClass(cls):
        """ Setup the test class. """
        pass

    @classmethod
    def tearDownClass(cls):
        """ Tear down the test class. """
        pass

    def setUp (self):
        """ Setup the test instance. """
        pass

    def tearDown (self):
        """ Tear down the test instance. """
        pass

    def testConstruction(self):
        """ Test the construction of a Agent instance. """

        # Setup the instance.
        instance = Agent()

        # Check it's instance.
        self.assertIsInstance(instance, Agent)

    def testDivide(self):
        """ Test the cell division. """

        # Setup the instance.
        cell0 = Agent()

        # Divide the cell.
        cell0.divide()

        self.assertEqual(cell0.number_of_copies, 2)

    def testMutation(self):
        """ Test the cell mutation. """

        # Setup the instance.
        cell0 = Agent()

        # Divide the cell.
        mutant = cell0.mutate(date=1,
                              mutation=1,
                              division_rate=0.2,
                              death_rate=1.0,
                              )

        # Check it's a clone.
        self.assertIsInstance(mutant, Agent)

        self.assertEqual(cell0.cell_id, mutant.cell_id)
        self.assertNotEqual(cell0.mutation_id, mutant.mutation_id)
        self.assertEqual(cell0.parent_mutation_ids, mutant.parent_mutation_ids[:-1])
        self.assertEqual(mutant.number_of_copies, 1)
        self.assertEqual(mutant.division_rate, 0.2)
        self.assertEqual(mutant.death_rate, 1.0)

    def testDie(self):
        """ Test the 'die' method."""

        # Setup the instance.
        cell0 = Agent()

        # Remove one cell.
        cell0.die()

        self.assertEqual(cell0.number_of_copies, 0)


if __name__ == "__main__":

    unittest.main()

