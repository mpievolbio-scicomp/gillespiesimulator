"""
    :module ParametersTest:
        Module to host the test class for the Gillespie simulation test
"""

import unittest

from GillespieSimulator.Parameters import Parameters


class ParametersTest(unittest.TestCase):
    """ :class: Test class for the Parameters class. """

    @classmethod
    def setUpClass(cls):
        """ Setup the test class. """
        pass

    @classmethod
    def tearDownClass(cls):
        """ Tear down the test class. """
        pass

    def setUp(self):
        """ Setup the test instance. """
        pass

    def tearDown(self):
        """ Tear down the test instance. """
        pass

    def testConstruction(self):
        """ Test the construction of a Parameters instance. """

        # Setup the instance.
        parameters = Parameters()

        # Check it's instance.
        self.assertIsInstance(parameters, Parameters)

    def test_division_rate(self):
        """ Test default and special values handling of the `division_rate`
        parameter."""

        parameters = Parameters()

        self.assertEqual(parameters.division_rate, 1.0)
        self.assertEqual(parameters.division_rate_fwhm, 0.0)

        # Value is taken from pdf_rms if not set.
        parameters = Parameters(
            division_rate=0.1,
            pdf_mean=0.5
        )

        self.assertEqual(parameters.division_rate, 0.1)
        self.assertEqual(parameters.division_rate_fwhm,
                         parameters.pdf_rms/0.425)

        # Value is taken if set.
        parameters = Parameters(division_rate=0.1,
                                pdf_mean=0.5,
                                division_rate_fwhm=0.01
                                )

        self.assertEqual(parameters.division_rate_fwhm, 0.01)

    def test_death_rate(self):
        """ Test default and special values handling of the `death_rate`
        parameter."""

        parameters = Parameters()

        self.assertEqual(parameters.death_rate, 0.0)
        self.assertEqual(parameters.death_rate_fwhm, 0.0)

        # Value is taken from pdf_rms if not set.
        parameters = Parameters(
            death_rate=0.1,
            pdf_mean=0.5
        )

        self.assertEqual(parameters.death_rate, 0.1)
        self.assertEqual(parameters.death_rate_fwhm,
                         parameters.pdf_rms/0.425)

        # Value is taken if set.
        parameters = Parameters(death_rate=0.1,
                                pdf_mean=0.5,
                                death_rate_fwhm=0.01
                                )

        self.assertEqual(parameters.death_rate_fwhm, 0.01)

    def test_mutation_probability(self):
        """ Test default and special values handling of the `mutation_probability`
        parameter."""

        parameters = Parameters()

        self.assertEqual(parameters.mutation_probability, 0.0)
        self.assertEqual(parameters.mutation_probability_fwhm, 0.0)

        # Value is taken from pdf_rms if not set.
        parameters = Parameters(
            mutation_probability=0.1,
            pdf_mean=0.5
        )

        self.assertEqual(parameters.mutation_probability, 0.1)
        self.assertEqual(parameters.mutation_probability_fwhm,
                         parameters.pdf_rms/0.425)

        # Value is taken if set.
        parameters = Parameters(mutation_probability=0.1,
                                pdf_mean=0.5,
                                mutation_probability_fwhm=0.01
                                )

        self.assertEqual(parameters.mutation_probability_fwhm, 0.01)




if __name__ == "__main__":

    unittest.main()
