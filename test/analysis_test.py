
""" :module analysis_test: Module to test the run analysis utilities. """

import codecs
import json
import logging
import multiprocessing
import numpy
import os
import shutil
import unittest


from GillespieSimulator import analysis

logging.getLogger().setLevel(level=logging.WARNING)


class AnalysisTest(unittest.TestCase):
    """ :class: Test class for the analysis utilities. """

    @classmethod
    def setUpClass(cls):
        """ Setup the test class. """
        pass

    @classmethod
    def tearDownClass(cls):
        """ Tear down the test class. """
        pass

    def setUp(self):
        """ Setup the test instance. """
        self.__dirs_to_remove = []
        self.__files_to_remove = []

    def tearDown(self):
        """ Tear down the test instance. """
        for f in self.__files_to_remove:
            if os.isfile(f):
                os.remove(f)

        for d in self.__dirs_to_remove:
            if os.path.isdir(d):
                shutil.rmtree(d)

    def test_reduced_competition_matrix(self):


        comp_matrix = numpy.array([
            [.1, .5, .3],
            [.2, .9, .1],
            [.4, .5, .8]
        ])

        snapshot = {
            "data": {
                "competition_matrix": comp_matrix,
                "cells" : [
                    {"cell_id": 0, "mutation_id": 0, },
                    {"cell_id": 1, "mutation_id": 0, },
                    {"cell_id": 0, "mutation_id": 1, },
                ]
            }
        }

        reduced_comp_matrix = analysis.reduced_competition_matrix(snapshot)

        expected = numpy.array([
            [.8, .5],
            [.2, .9]
        ])

        self.assertEqual(numpy.linalg.norm(reduced_comp_matrix - expected), 0.)

if __name__ == "__main__":
    unittest.main()
